import torch
import torch.nn as nn
import torch.utils.model_zoo as model_zoo

from IPython.core import debugger
debug = debugger.Pdb().set_trace



class Global(nn.Module):
    def __init__(self):
        super(Global, self).__init__()
                                                           
    def forward(self, x):
                    
        return out


class Neighbor(nn.Module):
    def __init__(self, can):
        super(Neighbor, self).__init__()
        
        
    def forward(self, x):
       
        return y


class Encoder(nn.Module):    
    def __init__(self, can, obs):
       
    def forward(self, x):
       
        return y
        

class Generator(nn.Module):
    def __init__(self, can=20, obs=32):
        super(Generator, self).__init__()

    def forward(self, x):
        
        return y


class Decoder(nn.Module):
    def __init__(self, num):
        super(Decoder, self).__init__()

    def forward(self, x):

        return d1, d2, d3, out


class TomoNet(nn.Module):
    def __init__(self, can=20, obs=32):
        super(TomoNet, self).__init__()
        self.encoder = Encoder(can, obs)
        self.generator = Generator(can, obs)
        self.decoder = Decoder(can*obs)
    def forward(self, x, p=0.2, training=True):
        pre_feature = self.encoder(x)        
        feature = self.generator(pre_feature)
        feature = nn.functional.dropout(feature, p=p, training=training)
        d1, d2, d3, out = self.decoder(feature)
        return feature, d1, d2, d3, out